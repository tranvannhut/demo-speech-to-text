function __log(e, data) {
  console.log(e + " " + (data || ''));
}
var audio_context;
var recorder;
function startUserMedia(stream) {
  var input = audio_context.createMediaStreamSource(stream);
  __log('Media stream created.');
  // Uncomment if you want the audio to feedback directly
  //input.connect(audio_context.destination);
  //__log('Input connected to audio context destination.');

  recorder = new Recorder(input);
  __log('Recorder initialised.');
}
function startRecording() {
  recorder && recorder.record();
  __log('Recording...');
}
async function stopRecording() {
  recorder && recorder.stop();
  __log('Stopped recording.');
  // create WAV download link using audio data blob
  const data = await speechToText();
  recorder.clear();
  return data;
}
function speechToText() {
  return new Promise((resolve, reject) => {
    return recorder && recorder.exportWAV(async function (blob) {
      __log("Call api");
      // var url = URL.createObjectURL(blob);
      // var li = document.createElement('li');
      // var au = document.createElement('audio');
      // var hf = document.createElement('a');

      // au.controls = true;
      // au.src = url;
      // hf.href = url;
      // hf.download = new Date().toISOString() + '.wav';
      // hf.innerHTML = hf.download;
      // li.appendChild(au);
      // li.appendChild(hf);
      // recordingslist.appendChild(li);

      const xhr = new XMLHttpRequest();
      xhr.onload = function (e) {
        if (this.readyState === 4) {
          console.log("Server returned: ", e.target.responseText);
          return resolve(e.target.response);
        }
      };
      const fd = new FormData();
      fd.append("audio_data", blob, "vai.wav");
      xhr.open("POST", "http://103.221.220.96:3000/api/speech-to-text", true);
      xhr.send(fd);
    });
  })

}

window.onload = function init() {
  try {
    // webkit shim
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
    window.URL = window.URL || window.webkitURL;

    audio_context = new AudioContext;
    __log('Audio context set up.');
    __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
  } catch (e) {
    alert('No web audio support in this browser!');
  }

  navigator.getUserMedia({ audio: true }, startUserMedia, function (e) {
    __log('No live audio input: ' + e);
  });

  const sleep = time => new Promise(resolve => setTimeout(resolve, time));
  const form = [{
    text: "Họ tên",
    val: "name"
  }, {
    text: "Giới tính",
    val: "gender"
  }, {
    text: "Địa chỉ",
    val: "address"
  }, {
    text: "Số điện thoại",
    val: "phone"
  }]
  let index = -1
  $('html')
    .on('click', '.btn-recorder', async function (e) {
      e.preventDefault();
      $('.btn-recorder').prop('disabled', true);
      $('.btn-recorder').text("Đang xử lý");
      index += 1;
      const currentForm = form[index];
      $(".text-current").text("Nhập " + currentForm.text);
      if (index === form.length - 1) {
        index = -1;
      }
      startRecording();
      let time = 5;
      const interval = setInterval(async (e) => {
        time -= 1;
        $(".text-time").text("Kết thúc sau " + time + "s")
        if (time === 0) {
          $(".text-time").text("Kết thúc sau 5s")
          clearInterval(interval);
          const text = await stopRecording();
          $("#" + currentForm.val).val(text.toString().replace(/"/g, ""));
          $(".text-current").text("Nhập " + form[index + 1].text);
          $('.btn-recorder').prop('disabled', false);
          $('.btn-recorder').text("Bắt đầu");
        }
      }, 1000)
    })

};
const sleep = time => new Promise(resolve => setTimeout(resolve, time));

async function handle(currentVal) {
  // e.preventDefault();
  // const currentForm = form.filter(e => e.val === currentVal);
  const btnSelector = '.btn-' + currentVal;
  startRecording();
  $(btnSelector).prop('disabled', true);
  $(btnSelector).text("Nói đi ...");
  await sleep(5000);
  $(btnSelector).text("Đang xử lý");
  const text = await stopRecording();
  $("#" + currentVal).val(text.toString().replace(/"/g, ""));
  $(btnSelector).prop('disabled', false);
  $(btnSelector).text("Ghi âm");
}
