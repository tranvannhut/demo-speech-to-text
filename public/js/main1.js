const recordAudio = () =>
  new Promise(async resolve => {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    const mediaRecorder = new MediaRecorder(stream);
    const audioChunks = [];

    mediaRecorder.addEventListener("dataavailable", event => {
      audioChunks.push(event.data);
    });

    const start = () => mediaRecorder.start();

    const stop = () =>
      new Promise(resolve => {
        mediaRecorder.addEventListener("stop", () => {
          mediaRecorder.exportWAV((blob) => console.log(blob));
          const audioBlob = new Blob(audioChunks);
          const audioUrl = URL.createObjectURL(audioBlob);
          const audio = new Audio(audioUrl);
          console.log(audioBlob)

          const xhr = new XMLHttpRequest();
          xhr.onload = function (e) {
            if (this.readyState === 4) {
              console.log("Server returned: ", e.target.responseText);
            }
          };
          const fd = new FormData();
          fd.append("audio_data", audioBlob, "test.wav");
          xhr.open("POST", "/api/speech-to-text", true);
          xhr.send(fd);

          const play = () => audio.play();
          resolve({ audioBlob, audioUrl, play });
        });

        mediaRecorder.stop();
      });

    resolve({ start, stop });
  });

const sleep = time => new Promise(resolve => setTimeout(resolve, time));

(async () => {
  const recorder = await recordAudio();
  recorder.start();
  await sleep(3000);
  const audio = await recorder.stop();
  audio.play();
})();