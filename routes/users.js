﻿'use strict';
const express = require('express');
const router = express.Router();

const multer = require('multer');
const fs = require('fs');

function saveFileViaRequest(req) {
    return new Promise((resolve, _reject) => {
        const file = req.files[0];
        const writeStream = fs.createWriteStream(file.originalname);
        writeStream.write(file.buffer);
        writeStream.on("finish", () => {
            return resolve(file.originalname);
        });
        writeStream.end();
    });

}

function syncRecognize(filename, encoding, sampleRateHertz, languageCode) {

    const request = {
        encoding,
        sampleRateHertz,
        languageCode,
    }

    return client.recognize(filename, request)
}

/* GET users listing. */
router.post('/speech-to-text', multer().any(), async function (req, res) {
    // const fileName = await saveFileViaRequest(req);
    const speech = require('@google-cloud/speech');
    const client = new speech.SpeechClient();
    const file = fs.readFileSync('test.mp3');
    const audioBytes = req.files[0].buffer.toString("base64");
    const audio = { content: audioBytes };
    const config = { encoding: 'LINEAR16', sampleRateHertz: 44100, languageCode: 'vi-VN', audioChannelCount: 2 };
    // Detects speech in the audio file
    return client
        .recognize({ audio: audio, config: config })
        .then(data => {
            const response = data[0];
            const transcription = response.results
                .map(result => result.alternatives[0].transcript)
                .join('\n');
            console.log(`Transcription: ${transcription}`);
            return res.json(transcription)
        })
        .catch(err => {
            config.sampleRateHertz = 8000;
            return client
                .recognize({ audio: audio, config: config })
                .then(data => {
                    const response = data[0];
                    const transcription = response.results
                        .map(result => result.alternatives[0].transcript)
                        .join('\n');
                    console.log(`Transcription: ${transcription}`);
                    return res.json(transcription)
                })
                .catch(err => {
                    console.error('ERROR:', err);
                    return res.json(err)
                });
        });
});

module.exports = router;
